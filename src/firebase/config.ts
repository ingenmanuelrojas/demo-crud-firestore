import firebase from 'firebase/app';
import 'firebase/firestore';

const firebaseConfig = {
    apiKey: "AIzaSyAIvj8uCb9WyD-jygHlCGfZYRTYlbdPg2I",
    authDomain: "sql-demos-f1d05.firebaseapp.com",
    databaseURL: "https://sql-demos-f1d05.firebaseio.com",
    projectId: "sql-demos-f1d05",
    storageBucket: "sql-demos-f1d05.appspot.com",
    messagingSenderId: "714394013150",
    appId: "1:714394013150:web:9986a7814525773c67214f"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);


export default firebase.firestore();