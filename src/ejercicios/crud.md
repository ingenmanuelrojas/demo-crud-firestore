#referencia al docuemnto
const usuariosRef = db.collection('usuarios');

#OBJETO
const usuario={
    nombre   : 'Susana',
    activo   : false,
    fechaNaci: 0,
    salario: 2000
}


/*INSERT USUARIOS*/
usuariosRef
    .add(usuario)
    .then(docRef => {
        console.log(docRef)    
    }).catch(e => console.log('error', e))

/*UPDATE USUARIOS */
usuariosRef
    .doc('8jx5ay1pbJ1qs7Fqm2ib')
    .update({
        activo:true
    });

/*DELETE USUARIOS */
usuariosRef
    .doc('KeyvT6BC3Ez7cTos7Ejk')
    .delete()
    .then( () => console.log('Borrado') )
    .catch( e => console.log('error',e))

/*SELECT * FROM (el snapshot es un socket) */
    usuariosRef
        .onSnapshot(snap => {

            retornaDocumentos(snap);

        }) 


// Select con get (no es socket)
    usuariosRef.get().then(snap => retornaDocumentos(snap))

/*
    select * from usuarios where activo = true
*/
    usuariosRef.where('activo', '==', false).get().then(snap => retornaDocumentos(snap))
    usuariosRef.where('salario', '>', 1800).get().then(snap => retornaDocumentos(snap))
    usuariosRef
        .where('salario', '>', 1800)
        .where('salario', '<', 2300)
        .get()
        .then(snap => retornaDocumentos(snap))

/*
select * from usuarios where salario > 1800 and  activo = true
*/
    usuariosRef
        .where('salario', '>', 1800)
        .where('activo', '==', true)
        .get().then(snap => retornaDocumentos(snap))